#define FUSE_USE_VERSION 26

#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>

#define STORAGE_PATH "/home/maximus/fs_code/fs"
#define FRAGMENT_COUNT 100000
#define FRAGMENT_SIZE 1024
#define MAX_NUM_FILES 32



typedef struct fnode_struct {
	char name[64]; 
	int start;			//number of start fragment
	int size;			
	int isDir;			//is directory 
	int isFill;			
} fnode;

typedef struct fsystem_struct {
	fnode node[MAX_NUM_FILES];
	int fragment[FRAGMENT_COUNT];
} fsystem;

//----------------------- filesystem ---------------------
fsystem fs; 
//---------------------------------------------------------

// init clear fs in memory
void fillFsEmptyValues() {
	int i = 0;
	int n = sizeof(fs.node) / sizeof(fs.node[0]);
	while (i < n) {
		memset(fs.node[i].name, '\0', 64);
		fs.node[i].start = -1;
		fs.node[i].size = 0;
		fs.node[i].isDir = 0;
		fs.node[i].isFill = 0;	
		i++;
	}

	i = 0;
	while (i < FRAGMENT_COUNT) {
		fs.fragment[i++] = -2;
	}
}

int createNewFs() {
	FILE *f = fopen(STORAGE_PATH, "w+");

	// place for file node
	char *buf = (char *)malloc(sizeof(fnode));
	memset(buf, '\0', sizeof(fnode));
	int i = 0;
	while (i < MAX_NUM_FILES) {
		fwrite(buf, sizeof(fnode), 1, f);
		i++;
	}
	free(buf);

	// place for fragments info
	buf = (char *)malloc(sizeof(int));
	memset(buf, '\0', sizeof(int));
	i = 0;
	while (i < FRAGMENT_COUNT) {
		fwrite(buf, sizeof(int), 1, f);
		i++;
	}
	free(buf);

	// place for fragment data
	buf = (char *)malloc(FRAGMENT_SIZE);
	memset(buf, '\0', FRAGMENT_SIZE);
	i = 0;
	while (i < FRAGMENT_COUNT) {
		fwrite(buf, FRAGMENT_SIZE, 1, f);
		i++;
	}
	free(buf);

	fclose(f);
	fillFsEmptyValues();
	addFile("/", 0, 1);
}

int findEmptyNode() {
	int i = 0;
	int n = sizeof(fs.node) / sizeof(fs.node[0]);
	while (i < n) {
		if (!fs.node[i].isFill) 
			return i;
		i++;
	}
	return -1; // not found	
}

int findEmptyFragment() {
	int i = 0;
	while (i < FRAGMENT_COUNT) {
		if (fs.fragment[i] == -2)
			return i;
		i++;
	}
	return -1; // not found
}

// get node by number 
fnode *getNodeByNumber(int k) {
	return &fs.node[k];
}

// write node[k] to fs file
int writeNode(int k) {
	FILE *f = fopen(STORAGE_PATH, "r+");
	fseek(f, k * sizeof(fnode), SEEK_SET);
	fwrite(getNodeByNumber(k), sizeof(fnode), 1, f); ////
	fclose(f);
	return 0;
}

// write fragment[]
int writeFragments(fnode *node) {
	FILE *f = fopen(STORAGE_PATH, "r+");
	int i = node->start;

	do {
		fseek(f, sizeof(fnode) * MAX_NUM_FILES + sizeof(int) * i, SEEK_SET);
		fwrite(&fs.fragment[i], sizeof(int), 1, f);
		i = fs.fragment[i];
	}
	while (i != -1);

	fclose(f);
	return 0;
}


int writeData(fnode *node, const char *data, int size, int offset) {
	if (size == 0) return 0;
	FILE *f = fopen(STORAGE_PATH, "r+");

	int i = node->start, j = offset / FRAGMENT_SIZE;
	while (j-- > 0) i = fs.fragment[i];
	
	int left = size;
	int count = 0;
	int off = offset % FRAGMENT_SIZE;

	int skip = sizeof(fnode) * MAX_NUM_FILES + sizeof(int) * FRAGMENT_COUNT; // node & fragments

	while (left > 0) { // there is something to write		
		if (off + left > FRAGMENT_SIZE)
			count = FRAGMENT_SIZE - off;
		else
			count = left;

		fseek(f, skip + i * FRAGMENT_SIZE + off, SEEK_SET);
		fwrite(data + size - left, 1, count, f);

		if (off + count == FRAGMENT_SIZE) {
			if (fs.fragment[i] >= 0) i = fs.fragment[i];
			else {
				int k = findEmptyFragment();
				if (k == -1) return -1;
				fs.fragment[i] = k;
				fs.fragment[k] = -1;
				i = k;
			}
		}

		left = left - count;
		off = 0;
	}

	node->size = size + offset;

	writeFragments(node);
	fclose(f);
	return size;	
}

// read data
int readData(fnode *node, char **data) {
	if (node == NULL) return -1;
	FILE *f = fopen(STORAGE_PATH, "r");
	char *buf = NULL;
	int size = node->size;
	buf = (char *)malloc(size);
	int i = node->start, k = 0, count;
	int skip = sizeof(fnode) * MAX_NUM_FILES + sizeof(int) * FRAGMENT_COUNT; // node & fragments
	while (i != -1) {
		int left = size - k * FRAGMENT_SIZE;
		if (left >= FRAGMENT_SIZE) 
			count = FRAGMENT_SIZE;
		else 
			count = left;
		fseek(f, skip + i * FRAGMENT_SIZE, SEEK_SET);
		fread(buf + k * FRAGMENT_SIZE, 1, count, f);
		i = fs.fragment[i];
		k++;
	}
	fclose(f);
	*data = buf;
	return size;

}

int readFile(fnode *node, char **buf, int size, int offset) {
	if (size == 0) return 0;

	int s = node->size;
	if (offset > s) return 0;
	if (size < s) s = size;

	FILE *f = fopen(STORAGE_PATH, "r");

	int i = node->start, j = offset / FRAGMENT_SIZE;
	while (j-- > 0) i = fs.fragment[i];
	
	int left = s;
	int count = 0;
	int off = offset % FRAGMENT_SIZE;

	char *data = (char *)malloc(s); 

	int skip = sizeof(fnode) * MAX_NUM_FILES + sizeof(int) * FRAGMENT_COUNT; // node & fragments

	while (left > 0) { // there is something to read		
		if (off + left > FRAGMENT_SIZE)
			count = FRAGMENT_SIZE - off;
		else
			count = left;

		fseek(f, skip + i * FRAGMENT_SIZE + off, SEEK_SET);
		fread(data + s - left, 1, count, f);

		if (off + count == FRAGMENT_SIZE) {
			i = fs.fragment[i];
		}

		left = left - count;
		off = 0;
	}
	fclose(f);
	*buf = (char *)data;

	return s;	
}

int getFileNodeNumber(char *data, char *name, int size) {
	int i = 0;
	int n = size / sizeof(int);
	while (i < n) {
		if (strcmp(fs.node[((int *)data)[i]].name, name) == 0)
			return ((int *)data)[i];
		i++;	
	}
	return -1;
}

//get node by filepath
int getNode(const char *path, fnode **node) {
	char *fpath = (char*)malloc(strlen(path));
	strcpy(fpath, path);
	printf("%s\n", fpath);
	printf("p-3\n");
	if (fpath && strcmp("/", fpath) == 0) { //root
		printf("p-2\n");
		*node = getNodeByNumber(0); 	
		printf("p-1\n");
		return 0;
	}

	fnode *m = NULL;
	char *p;

	p = fpath;
	printf("p0\n");
	if (*p++ == '/')
		m = getNodeByNumber(0);
	else return -1;
	printf("p0.5\n");
	char *data, *s;
	char name[64];
	memset(name, '\0', 64);

	int k = -1, size;
	printf("p1\n");
	while (p - fpath < strlen(fpath)) {
		if (m->size == 0)
			return -1;
		printf("p2\n");
		size = readData(m, &data);
		s = p;
		p = strchr(p, '/');
		if (p != NULL) {
			p = p + 1;
			strncpy(name, s, p - s - 1);			
		}
		else {
			strncpy(name, s, fpath + strlen(fpath) - s);
			p = fpath + strlen(fpath);		
		}
		k = getFileNodeNumber(data, name, size);
		if (k == -1) return -1;
		m = getNodeByNumber(k);
		memset(name, '\0', 64);
		free(data);
		printf("p3\n");
	}

	*node = m;
	return k;
}

// returns node number
int addFile(char* name, int size, int isDir) {
	fnode *node = NULL;
	int k = findEmptyNode();
	if (k == -1) return -1;
	node = getNodeByNumber(k);
	int start = findEmptyFragment();
	if (start == -1) return -1;

	// write info to node
	strcpy(node->name, name);
	node->start = start;
	node->size = size;
	node->isDir = isDir;
	node->isFill = 1;

	fs.fragment[start] = -1;
	writeNode(k);
	writeFragments(node);
	return k;
}

int createFile(const char* path, int isDir) {
	fnode *node;
	char *dir, *name;
	char *data, *moddata;

	name = strrchr(path, '/');
	if (name == NULL) {
		strcpy(name, path);
		dir = (char *)malloc(2);
		strcpy(dir, "/\0");
	} else {
		name = name + 1;
		int len = strlen(path) - strlen(name) ;
		dir = (char *)malloc(len + 1);
		strncpy(dir, path, len);
		dir[len] = '\0';
	}
	int nodeNum = getNode(dir, &node);
	int size = readData(node, &data);

	moddata = (char*)malloc(size + sizeof(int));
	memcpy(moddata, data, size);

	int k = addFile(name, 0, isDir);
	((int*)moddata)[size/sizeof(int)] = k;

	writeData(node, moddata, size + sizeof(int), 0);
	node->size = size + sizeof(int);	
	writeNode(nodeNum);

	free(moddata);
	free(dir);
	return 0;
}

char *getDirPath(const char* path) {
	char *dir;
	char *p = strrchr(path, '/');
	if (p != NULL) {
		int len = strlen(path) - strlen(p);
		if (len != 0) {
			dir = (char *)malloc(len + 1);
			strncpy(dir, path, len);	
			dir[len] = '\0';
		} else {
			dir = (char *)malloc(2);
			strcpy(dir, "/\0");
		}
	} else {
		dir = (char *)malloc(2);
		strcpy(dir, "/\0");
	}
	return dir;
}

int removeFile(const char* path) {
	fnode *fileNode, *dirNode;
	char *data, *moddata;
	char *dir = getDirPath(path);

	printf("removeFile: dir = %s\t path = %s\n", dir, path);

	int dirNodeNum = getNode(dir, &dirNode);
	int fNodeNum = getNode(path, &fileNode);
	int size = readData(dirNode, &data);

	moddata = (char *)malloc(size - sizeof(int));
	int i = 0, j = 0;
	while (i < size / sizeof(int)) {
		if (((int *)data)[i] != fNodeNum)
			((int *)moddata)[j++] = ((int *)data)[i];
		i++; 
	}

	writeData(dirNode, moddata, size, 0);
	dirNode->size = size - sizeof(int);
	writeNode(dirNodeNum);

	free(data);
	free(dir);

	return 0;
}

int openFile(const char* path) {
	fnode *node;
	int nodeNum = getNode(path, &node);
	if (nodeNum == -1) return -1;
	return 0;
}

static int fs_getattr(const char* path, struct stat *stbuf) {
	int res = 0;
	printf("fs_getattr: path = %s\n", path);
	fnode *node;
	if (getNode(path, &node) == -1)
		res = -ENOENT;
	printf("fs_getattr: getNode end\n");
	memset(stbuf, 0, sizeof(struct stat));
	printf("fs_getattr: memset end\n");
    if(node->isDir) {
		stbuf->st_mode = S_IFDIR | 0755;
		stbuf->st_nlink = 2;
    }
	else {
    	stbuf->st_mode = S_IFREG | 0444;
   	 	stbuf->st_nlink = 1;
    	stbuf->st_size = node->size;
	}
	stbuf->st_mode = stbuf->st_mode | 0777;
	printf("fs_getattr: end res:%d\n", res);
    return res;
}

static int fs_mkdir(const char* path, mode_t mode) {
	if (createFile(path, 1) != 0)
		return -1;
	return 0;
}

static int fs_create(const char *path, mode_t mode, struct fuse_file_info *finfo) {
	if (createFile(path, 0) != 0)
		return -1;
	return 0;
}

static int fs_rmdir(const char *path) {
	int res = removeFile(path);
	if (res != 0) return -1;
	return 0;
}

static int fs_unlink(const char *path) {
	int res = removeFile(path);
	if (res != 0) return -1;
	return 0;
}

static int fs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, 
		off_t offset, struct fuse_file_info *fi) {
	printf("fs_readdir: start\n");
    	filler(buf, ".", NULL, 0);
    	filler(buf, "..", NULL, 0);

	fnode *node;
	int nodeNum = getNode(path, &node);
	printf("fs_readdir: nodeNum = %d\n", nodeNum);
	if (nodeNum == -1) return -ENOENT;

	char *data;
	int size = readData(node, &data);
	printf("fs_readdir: size = %d\n", size);

    	int i = 0, n = size/sizeof(int);
	while (i < n) {
printf("fs_readdir: ((int*)data)[%d] = %d\nname = %s\n", i, ((int*)data)[i], fs.node[((int*)data)[i]].name);
		filler(buf, fs.node[((int*)data)[i]].name, NULL, 0);
		i++;
	}

    	return 0;
}

static int fs_read(const char *path, char *buf, size_t size, off_t offset,
                      struct fuse_file_info *fi) {
	printf("fs_read: start\n");
	fnode *node;
	int nodeNum = getNode(path, &node);
	if (nodeNum == -1) return -ENOENT;
	char *data;
	int s = readFile(node, &data, size, offset);
	if (s == -1) 
		return -ENOENT;
	memcpy(buf, data, s);
    	return s;
}

static int fs_open(const char *path, struct fuse_file_info *fi)
{
	printf("fs_open: start\n");
	if (openFile(path) == -1) return -ENOENT;
	return 0;
}

static int fs_opendir(const char *path, struct fuse_file_info *fi)
{
	printf("fs_opendir: start\n");
	if (openFile(path) == -1) return -ENOENT;
	return 0;
}

static int fs_write(const char *path, const char *buf, size_t nbytes, 
			off_t offset, struct fuse_file_info *fi) {
	fnode *node;
	int nodeNum = getNode(path, &node);
	if (nodeNum == -1) return -ENOENT;
	int s = writeData(node, buf, nbytes, offset);
	if (s != -1) {
		writeNode(nodeNum);
		return s;
	}
	return -1;
}

static int fs_rename(const char* from, const char* to) {
	fnode *node, *dirNode;

	int nodeNum = getNode(from, &node);
	if (nodeNum == -1) return -ENOENT;
	removeFile(from);

	char *dir, *name;
	name = strrchr(to, '/');
	if (name == NULL) {
		strcpy(name, to);
		dir = (char *)malloc(2);
		strcpy(dir, "/\0");
	} else {
		name = name + 1;
		int len = strlen(to) - strlen(name) ;
		dir = (char *)malloc(len + 1);
		strncpy(dir, to, len);
		dir[len] = '\0';
	}

	memset(node->name, '\0', 64);
	strncpy(node->name, name, strlen(name));
	writeNode(nodeNum);

	char *data;
	char *moddata;

	int dirNodeNum = getNode(dir, &dirNode);
	int size = readData(dirNode, &data);

	moddata = (char*)malloc(size + sizeof(int));
	memcpy(moddata, data, size);
	((int*)moddata)[size/sizeof(int)] = nodeNum;

	writeData(dirNode, moddata, size + sizeof(int), 0);
	dirNode->size = size + sizeof(int);	
	writeNode(dirNodeNum);

	return 0;
}

static void *fs_init(struct fuse_conn_info *fi) {
	createNewFs();
	return 0;
}

//allowed fuse operation
static struct fuse_operations fs_oper = {
	.getattr	= fs_getattr,
	.readdir	= fs_readdir,
	.mkdir		= fs_mkdir,
	.rmdir		= fs_rmdir,
	.opendir	= fs_opendir,
	.create		= fs_create,
	.open		= fs_open,
	.read		= fs_read,
	.write		= fs_write,
	.unlink		= fs_unlink,
	.rename		= fs_rename,
	.init		= fs_init,	
};

int main(int argc, char *argv[])
{
	printf("startFuse\n");
	return fuse_main(argc, argv, &fs_oper, NULL);
}